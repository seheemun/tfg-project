import azure.cosmos.documents as documents
import azure.cosmos.cosmos_client as cosmos_client
import azure.cosmos.exceptions as exceptions
from abc import ABC, abstractmethod
from config.config import DefaultConfig



# DAO interface como clase abstracta
class FeedbackDaoInterface(ABC):
    @abstractmethod
    def create(self, feedback):
        pass


class FeedbackDTO:
    def __init__(self,id,feedback,user_rating,fb_date):
        self.id = id
        self.feedback = feedback
        self.user_rating = user_rating
        self.date = fb_date
    
    def get_structure(self):
        id = self.id
        feedback = self.feedback
        user_rating = self.user_rating
        fb_date = self.date    
        return {'id' : id,
                'partitionKey' : 'feedback',
                'feedback' : feedback,
                'user_rating' : user_rating,
                'fb_date' : fb_date,
                }


class FeedbackDAOImp(FeedbackDaoInterface):
    def __init__(self): 
        settings = DefaultConfig()
        client = cosmos_client.CosmosClient(settings.DB_HOST, {'masterKey': settings.DB_MASTER_KEY}, user_agent="CosmosDBPythonTFG", user_agent_overwrite=True)
        db = client.get_database_client(settings.DB_ID)
        self.container = db.get_container_client(settings.DB_CONTAINER_ID)#


    def create(self, feedback_dto):
        self.container.create_item(body=feedback_dto)



