from datetime import date
import requests
import csv
import re
from abc import ABC, abstractmethod

# DAO interface como clase abstracta
class INESearchDaoInterface(ABC):

    @abstractmethod
    def get(self, search_dto, years):
        pass
    
    @abstractmethod
    def create(self, list_vals, dict_id, search_dto):
        pass


class INESearchDTO:
    def __init__(self,codes = None, years = None, values =  None, val_aux = None, rows = 100):
        self.codes = codes
        self.years = years
        self.values = values
        self.val_aux = val_aux
        self.rows = rows
    
    def get_structure_for_search(self):
        codes = self.codes
        rows = self.rows
        dto = []
        url_template = 'http://servicios.ine.es/wstempus/js/ES/DATOS_SERIE/{code}?nult={num_rows}'
        for ine_code in codes:
            dto.append(url_template.format(code=ine_code, num_rows=rows))

        return dto

    def get_structure_for_search_with_codes(self):

        dto = []
        rows = self.rows
        url_template = 'http://servicios.ine.es/wstempus/js/ES/DATOS_SERIE/{code}?nult={num_rows}'

        for list_code in (self.values):
            codes = list(list_code.keys())
            for ine_code in codes:
                dto.append([url_template.format(code=ine_code, num_rows=rows),ine_code])
            
        return dto
    
    def get_values(self):
        return self.values
    
    def get_val_aux(self):
        return self.val_aux


class INESearchDAOImp(INESearchDaoInterface):

    def get(self, search_dto, years):
        
        dict_values = {}
        ine_value = 0
        ine_values = []
        count_year = 0

        for url in search_dto:
            response = requests.get(url).json()
            if len(years) == 1:
                for i in response['Data']:
                    if str(i['Anyo']) in years:
                        ine_value += i['Valor']
                        dict_values[int(i['Anyo'])] = i['Valor']
            else:
                if re.search("P..", ' '.join(str(element) for element in years)):
                    for i in response['Data']:
                        if str(i['Anyo']) >= years[0] and str(i['Anyo']) <= years[-2]:
                            ine_value += i['Valor']
                            dict_values[int(i['Anyo'])] = i['Valor']
                elif "since" in years:
                    for i in response['Data']:
                        if str(i['Anyo']) <= years[0] and years[0] >= str(date.today().year):
                            ine_value += i['Valor']
                            dict_values[int(i['Anyo'])] = i['Valor']

                        if str(i['Anyo']) >= years[0] and years[0] <= str(date.today().year): 
                            ine_value += i['Valor']
                            dict_values[int(i['Anyo'])] = i['Valor']
                elif "before" in years:
                    for i in response['Data']:
                        if str(i['Anyo']) <= years[0]:
                            ine_value += i['Valor']
                            dict_values[int(i['Anyo'])] = i['Valor']
                else:
                    ine_values = [0 for _ in years]
                    for i in response['Data']:
                        if str(i['Anyo']) in years:
                            dict_values[i['Anyo']] = i['Valor']
                            ine_values[count_year] = int(i['Valor'])
                            count_year += 1

        if ine_values:
            return INESearchDTO(values=ine_values,val_aux=dict_values)
            #return ine_values, dict_values
        else:
            return INESearchDTO(values=[int(ine_value)],val_aux=dict_values)
            #return [int(ine_value)], dict_values
        
    
    def create(self, list_vals, dict_id, search_dto):

        # si el primer elemento del array de codigo está dentro del array infraction de ine_all_searches entonces
        # la descripción/Dato es "delitos" en lugar de "personas"
        for list_code in (list_vals):
            header = ['numero', 'anyo','dato']
            file_name= "data_plot_" + str(dict_id) + ".csv" 

            with open("data/"+file_name, 'w', encoding='UTF8', newline='') as f:
                writer = csv.writer(f,delimiter=',',escapechar=" ",quoting=csv.QUOTE_NONE) 
                writer.writerow(header)

                for searches in search_dto:
                    response = requests.get(searches[0]).json()
                    data = []
                    for i in response['Data']:
                        val = [str(i['Valor']) + ','+ str(i['Anyo']) + ',' + list_code.get(searches[1])]
                        data.append(val)
                    writer.writerows(data)