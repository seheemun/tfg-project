
from enum import Enum
from datetime import datetime

"""
Clase BotConversationState. Representa un estado de los que puede tener una conversación.
Hereda de clase Enum (Enumeración)
"""
class BotConversationState(Enum):
    FINISHED = 1
    ONGOING = 2
    PAUSE = 3


"""
Clase BotConversation. Representa una conversación con el bot.
"""
class BotConversation:

    """
    Constructor de la clase
    @param self: el propio objeto BotConversation
    @param timestamp: Última actividad del usuari@ en la conversación
    @param channel_id: Identificador del canal destino por el que se está utilizando el bot
    @param prompted_for_user_name: Indica si a lo largo de la conversación se ha preguntado por el nombre del usuario
    """
    def __init__(self, timestamp: datetime = None, channel_id: str = None) -> None:
        self.state = BotConversationState(3).value
        self.timestamp = timestamp
        self.channel_id = channel_id


    #-------- Getters ------------
    def get_state(self):
        return self.state
    
    def get_timestamp(self):
        return self.timestamp

    def get_channel_id(self):
        return self.channel_id

    #-------- Setters ------------
    def set_state(self, state : int):
        self.state = state

    def set_timestamp(self, timestamp : datetime):
        self.timestamp = timestamp
    
    def set_channel_id(self, channel_id : str):
        self.channel_id = channel_id


    
