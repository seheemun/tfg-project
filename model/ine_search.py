
ine_all_searches = {
    "relationship" : ["conyugue","exconyugue","novio","exnovio","pareja de hecho","ex pareja de hecho","en separacion"],
    "nationality" : ["europa_sin_espana","espana","africa","america","asia","oceania"],
	"infraction" : ["homicidio","lesion","secuestro","amenaza","coaccion","tortura","agresion sexual","abuso sexual","allanar morada","injuria","dano","quebrantar condena","otros delitos"]
    ,"victima":[]
    ,"absuelto": []
    ,"condenado":[],
    "denunciado":[]
}

ine_code_description = {
    # busqueda principal
    "condenado": {"code" :["VGD16981"], "desc": "hombres condenados con sentencia firme por violencia de género", "short_desc": "condenados"},
    "victima": {"code" :["VGD25"], "desc": "mujeres víctimas de violencia de género", "short_desc": "víctimas"},
    "denunciado": {"code" :["VGD24"], "desc": "hombres denunciados por violencia de género", "short_desc": "denunciados"},
    "infraccion": {"code" :["VGD28"], "desc": "delitos de violencia de género", "short_desc": "infracciones"},
    "absuelto": {"code" :["VGD4018"], "desc": "hombres absueltos por delitos de violencia de género", "short_desc": "absueltos"},

    # lugar de nacimiento del denunciado
    "europa": {"code" :["VGD19846"], "desc": "hombres europeos denunciados por violencia de género", "short_desc": "Europa"},
    "espana": {"code" :["VGD19845"], "desc": "hombres españoles denunciados por violencia de género", "short_desc": "España"},
    "africa": {"code" :["VGD19842"], "desc": "hombres africanos denunciados por violencia de género", "short_desc": "África"},
    "america": {"code" :["VGD19841"], "desc": "hombres americanos denunciados por violencia de género", "short_desc": "América"},
    "asia": {"code" :["VGD19840"], "desc": "hombres asiáticos denunciados por violencia de género", "short_desc": "Asia"},
    "oceania": {"code" :["VGD19839"], "desc": "hombres oceánicos denunciados por violencia de género", "short_desc": "Oceanía"},
    "europa_sin_espana": {"code" :["VGD19844","VGD21654"], "desc": "hombres europeos no españoles denunciados por violencia de género", "short_desc": "Resto de Europa"},
    #"europa_sin_ue27": {"code" :["VGD21653"], "desc": "hombres del resto de europa denunciados por violencia de género"},
    "extranjero": {"code" :["VGD19839","VGD19842","VGD19841","VGD19840","VGD19844","VGD21654"], "desc": "hombres extranjeros denunciados por violencia de género"},

    # infracciones penales imputadas al condenado en asuntos con sentencia firme según tipo 
    "homicidio": {"code" :["VGD4270"], "desc": "infracciones por homicidio y sus formas", "short_desc": "Homicidio"},
    "lesion": {"code" :["VGD4261"], "desc": "infracciones por lesiones", "short_desc": "Lesiones"},
    "secuestro": {"code" :["VGD4252"], "desc": "infracciones por secuestro y detenciones ilegales", "short_desc": "Secuestro"},
    "amenaza": {"code" :["VGD4243"], "desc": "infracciones por amenazas", "short_desc": "Amenazas"},
    "coaccion": {"code" :["VGD4234"], "desc": "infracciones por coacción", "short_desc": "Coacción"},
    "tortura": {"code" :["VGD4225"], "desc": "infracciones por torturas e integridad moral", "short_desc": "Torturas"},
    "agresion sexual": {"code" :["VGD4216"], "desc": "infracciones por agresión sexual", "short_desc": "Agresión sexual"},
    "abuso sexual": {"code" :["VGD4207"], "desc": "infracciones por abuso sexual", "short_desc": "Abuso sexual"},
    "allanar morada": {"code" :["VGD4198"], "desc": "infracciones por allanamiento de morada", "short_desc": "Allanamiento de morada"},
    "injuria": {"code" :["VGD4189"], "desc": "infracciones por injurias", "short_desc": "Injurias"},
    "dano": {"code" :["VGD4180"], "desc": "infracciones por daños", "short_desc": "Daños"},
    "quebrantar condena": {"code" :["VGD417"], "desc": "infracciones por quebrantamiento de condena", "short_desc": "Quebrantamiento de condena"},
    "otros delitos": {"code" :["VGD4162"], "desc": "infracciones sin especificar", "short_desc": "Otros delitos"},

    # relacion con el denunciado
    "conyugue": {"code" :["VGD353"], "desc": "mujeres víctimas de su cónyugue","short_desc": "Cónyugue"},
    "exconyugue": {"code" :["VGD352"], "desc": "mujeres víctimas de su ex cónyugue","short_desc": "Ex cónyugue"},
    "novio": {"code" :["VGD351"], "desc": "mujeres víctimas de su novio","short_desc": "Novio"},
    "exnovio": {"code" :["VGD350"], "desc": "mujeres víctimas de su ex novio","short_desc": "Ex novio"},
    "pareja de hecho": {"code" :["VGD349"], "desc": "mujeres víctimas de su pareja de hecho","short_desc": "Pareja de hecho"},
    "ex pareja de hecho": {"code" :["VGD348"], "desc": "mujeres víctimas de su ex pareja de hecho","short_desc": "Ex pareja de hecho"},
    "en separacion": {"code" :["VGD19917"], "desc": "mujeres víctimas de su pareja cuando aun estaban en proceso de separación","short_desc": "En separación"},
    "inRelationship": { "code" :["VGD353","VGD351","VGD349"], "desc": "mujeres víctimas de sus parejas"}, 
    "exRelationship": {"code" :["VGD352","VGD350","VGD348","VGD19917"], "desc": "mujeres víctimas de sus ex parejas"},
}


"""
Clase INESearch. Representa una búsqueda a la base de datos del INE.
"""
class INESearch:

    """
    Constructor de la clase
    @param self: el propio objeto INESearch
    @param search: búsqueda del usuari@
    @param years: Año a aplicar la búsqueda del usuari@ 
    """
    
    def __init__(self, search: list = [], years: list = []):
        self.search = search
        self.years = years
        self.listed_values = {}
        self.description = ""
        self.relationship = ""
        self.nationality = ""

    #-------- Getters ------------
 
    def get_search(self):
        return self.search
    
    def get_years(self):
        return self.years

    def get_relationship(self):
        return self.relationship

    def get_nationality(self):
        return self.nationality

    def get_listed_values(self):
        return self.listed_values
    
    def get_description(self):
        return self.description
    
    #-------- Setters ------------   

    def set_search(self, search: list = []):
        self.search = search
    
    def set_years(self, years: list = []):
        self.years = years
    
    def set_relationship(self, relationship: str = ""):
        self.relationship = relationship

    def set_nationality(self, nationality: str = ""):
        self.nationality = nationality

    def set_listed_values(self, listed_values: dict = {}):
        self.listed_values = listed_values
    
    def set_description(self, description: str = ""):
        self.description = description