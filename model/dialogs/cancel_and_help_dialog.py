

from botbuilder.dialogs import (
    ComponentDialog,
    DialogContext,
    DialogTurnResult,
    DialogTurnStatus,
)
from botbuilder.ai.luis import LuisApplication, LuisRecognizer, LuisPredictionOptions
from botbuilder.schema import ActivityTypes, InputHints
from botbuilder.core import MessageFactory
from config.config import DefaultConfig
from model.response import Response

class CancelAndHelpDialog(ComponentDialog):
    def __init__(self, dialog_id: str):
        super(CancelAndHelpDialog, self).__init__(dialog_id)
        self.response = Response()
        self.recognizer = self.create_recognizer(DefaultConfig())
        self.initial_dialog_id = ""
        

    async def on_continue_dialog(self, inner_dialog_context: DialogContext) -> DialogTurnResult:
        result = await self.interrupt(inner_dialog_context)
        if result is not None:
            return result

        return await super(CancelAndHelpDialog, self).on_continue_dialog(inner_dialog_context)

    async def interrupt(self, inner_dialog_context: DialogContext) -> DialogTurnResult:
        if inner_dialog_context.context.activity.type == ActivityTypes.message:
            if inner_dialog_context.context.activity.text:
                text = inner_dialog_context.context.activity.text.lower()         

                if text in ("ayuda","manual de usuario","manual","help","terminos de uso","términos de uso","terminos","condiciones", "?"):
                    help_text = self.response.get_response("help")
                    help_message = MessageFactory.text(
                        help_text, help_text, InputHints.expecting_input
                    )
                    await inner_dialog_context.context.send_activity(help_message)
                    return DialogTurnResult(DialogTurnStatus.Waiting)

                if text in ("cancelar","cancela","stop","para","parar","exit","salir","cancel", "quit", "cancell","para","callate","cállate","adiós","chao","adios","bye"):
                    cancel_text = self.response.get_response("cancel")
                    cancel_message = MessageFactory.text(
                        cancel_text, cancel_text, InputHints.ignoring_input
                    )
                    await inner_dialog_context.context.send_activity(cancel_message)
                    return await inner_dialog_context.cancel_all_dialogs()
        return None


    def create_recognizer(self, configuration: DefaultConfig):
        luis_is_configured = (configuration.LUIS_APP_ID and configuration.LUIS_API_KEY and configuration.LUIS_API_HOST_NAME)

        if luis_is_configured:
            luis_application = LuisApplication(
                configuration.LUIS_APP_ID,
                configuration.LUIS_API_KEY,
                configuration.LUIS_API_HOST_NAME,
            )
            luis_option = LuisPredictionOptions(include_all_intents=True, include_instance_data=True, spell_check=True)
            return LuisRecognizer(luis_application, luis_option, True)
