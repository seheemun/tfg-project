from botbuilder.dialogs import (
    WaterfallDialog,
    WaterfallStepContext,
    DialogTurnResult,
)
from botbuilder.dialogs.prompts import (
    TextPrompt,
    ChoicePrompt,
    PromptOptions,
)
from botbuilder.core import MessageFactory, UserState


from model.user import UserProfile
from .cancel_and_help_dialog import CancelAndHelpDialog

from model.intent import Intent
from model.response import Response


class IntroAndHelpDialog(CancelAndHelpDialog):
    def __init__(self, user_state: UserState):
        super(IntroAndHelpDialog, self).__init__(IntroAndHelpDialog.__name__)
        self.user_profile_accessor = user_state.create_property("UserProfile")
        self.response = Response()

        self.add_dialog(
            WaterfallDialog(
                WaterfallDialog.__name__,
                [
                    self.show_help_step,
                    self.ask_name_step,                
                ],
            )
        )
        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(ChoicePrompt(ChoicePrompt.__name__))
        self.initial_dialog_id = WaterfallDialog.__name__


    async def show_help_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        await step_context.context.send_activity(MessageFactory.text(self.response.get_response("help")))
        #time.sleep(2) bug de emulator
        return await step_context.prompt(
            TextPrompt.__name__,
            PromptOptions(prompt=MessageFactory.text("Por cierto... ¿Cómo te llamas?😊")),
        )


    async def ask_name_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        
        if step_context.result:
            if len(step_context.result.strip()) != 0:
                if ' ' in step_context.result:
                    luis_result = await self.recognizer.recognize(step_context.context)
                    intent = self.recognizer.top_intent(luis_result, min_score = 0.40)
                    if intent == Intent.GET_USER_NAME.value:        
                        step_context.values["name"] = luis_result.entities.get("name")[0].capitalize()
                    else:
                        step_context.values["name"] = ""
                else:
                    step_context.values["name"] = step_context.result.capitalize()
        if step_context.values:
            user_profile = UserProfile(step_context.values["name"])
            # guardo datos
            await self.user_profile_accessor.set(step_context.context, user_profile)

        answer = self.response.get_response("initiate")
        await step_context.prompt(
            TextPrompt.__name__,
            PromptOptions(prompt=MessageFactory.text(answer + " " + step_context.values["name"])),
        )
        return await step_context.end_dialog()


  
