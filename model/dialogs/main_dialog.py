
import random
from azure.ai.language.questionanswering import models, QuestionAnsweringClient
from botbuilder.schema import (
    Attachment,
    CardAction,
    ActionTypes,
    CardImage,
    Attachment,HeroCard
)
from botbuilder.core import (
    MessageFactory,
    UserState,
    TurnContext,
    CardFactory
)
from model import Intent
from model.response import Response
import re

class MainDialog:

    def __init__(self, question_answering : QuestionAnsweringClient,
    user_state: UserState, turn_context : TurnContext, intent : Intent):
    
        self.question_anwering = question_answering
        self.last_output = None
        self.id_dict_chauv = []
        self.id_dict_tips = []
        self.user_state = user_state

        self.turn_context = turn_context
        self.intent = intent
        self.response = Response()

    def create_hero_card(self, answer : models.KnowledgeBaseAnswer) -> Attachment:

        answer_txt = answer.answer

        if len(re.findall("http.*[jpg|png|jpeg]", answer.answer)) > 0:
            img_url =  re.findall("http.*[jpg|png|jpeg]", answer.answer)[0] 
            if len(answer.answer.split('\n', 1)) > 0:
                # esto es para quitar la primera línea que contiene el nombre del par QnA y el enlace a la imagen
                answer_txt = answer.answer.split('\n', 1)[1]      
        else: 
            img_url = None   

        herocard = HeroCard(title="Esto te puede interesar 🧐",
            text = answer_txt,
            images = [
            CardImage(
                url=img_url
            )
            ],
            buttons=self.create_card_action(answer)
        )
        return MessageFactory.attachment(CardFactory.hero_card(herocard))


    def create_card_action(self, answer : models.KnowledgeBaseAnswer) -> CardAction: 
        suggested_actions_prompt = []
        for suggested_prompt in answer.dialog.prompts:
            suggested_actions_prompt.append(CardAction(title=suggested_prompt.display_text,
                type= ActionTypes.im_back, value=suggested_prompt.display_text))
            

        return suggested_actions_prompt

 
    # Procesa la intención del usuario
    async def process_intent(self):

        answer = ''
        qa_answer = None

        if self.intent == Intent.HELLO.value:
            answer = self.response.get_response("initiate")    

        elif self.intent == Intent.CANCEL.value:
            answer = "😶"

        elif self.intent == Intent.CRITICIZE.value:
            answer = self.response.get_response("apologize")  
        
        elif self.intent == Intent.HELP.value:
            answer = self.response.get_response("help") 

        elif self.intent == Intent.TERMS_OF_USE.value:
            answer = self.response.get_response("terms")  
        
        elif self.intent == Intent.COMPLIMENT.value:
            answer = self.response.get_response("thank")  

        elif self.intent == Intent.IDENTIFY_SEXISM.value:
            answer = await self.intent_sexism()

        elif self.intent == Intent.FEMINIST_TIP.value:
            answer = await self.intent_fem_tips()

        else:
            qa_answer = await self.run_question_answering(self.turn_context)


        # Si no ha habido coincidencia, devuelve mensaje de no haber ententendido   
        if answer:
            await self.turn_context.send_activity(answer)
        elif qa_answer:
            await self.turn_context.send_activity(qa_answer)
        else:
            await self.turn_context.send_activity(self.response.get_response("not_understood"))


    # Trata la itención identificar machismos
    async def intent_sexism(self):
        dict_chauvinism = self.response.get_response("identify_sexism", True) 
        lista_hombre = ['varón','varon','hombre','macho','chico','niño','novio','marido','esposo','hijo']
        #Control de respuestas, no se dará dos veces una misma respuesta, a no ser que ya se dado todas
        if len(self.id_dict_chauv) > 0:
            #Si la respuesta aun no se ha mostrado, se incluye como posible respuesta
            possible_answer = [entry['id'] for entry in dict_chauvinism if entry['id'] not in self.id_dict_chauv]
            if len(possible_answer) == 0:
                self.id_dict_chauv.clear()   
                possible_answer = [entry['id'] for entry in dict_chauvinism]
        else:   
            #Si ya se han mostrado todas las respuestas, se empieza de nuevo
            self.id_dict_chauv.clear()   
            possible_answer = [entry['id'] for entry in dict_chauvinism]

        if any(x in self.turn_context.activity.text.lower() for x in lista_hombre):
            #Cuando se pregunte específicamente por machismos en hombres y la respuesta esté dentro de las posibles
            arr_answer = [str(entry['id']) + '#' + entry['desc'] for entry in dict_chauvinism if any(x in entry['desc'] for x in lista_hombre) and (entry['id'] in possible_answer)]  
            if arr_answer is not None and len(arr_answer) > 0:                   
                aux_value = random.choice(arr_answer)
                key_value = int(aux_value.split('#')[0])
                answer = aux_value.split('#',1)[1]
            else:
                #Para la selección, no hay más respuestas que cumplan el criterio: Se devuelve cualquier respuesta
                key_value = random.choice(possible_answer)
                answer = next((sub for sub in dict_chauvinism if sub['id'] == key_value), None)['desc']
        else:
            #Respuesta comprendida como posible respuesta
            key_value = random.choice(possible_answer)
            answer = next((sub for sub in dict_chauvinism if sub['id'] == key_value), None)['desc']

        self.id_dict_chauv.append(key_value)
        return answer
        

    # Trata la intención de tips feministas
    async def intent_fem_tips(self):
        dict_tips = self.response.get_response("feminist_tip", True)
        #Control de respuestas, no se dará dos veces una misma respuesta, a no ser que ya se dado todas
        if len(self.id_dict_tips) > 0:
            #Si la respuesta aun no se ha mostrado, se incluye como posible respuesta
            possible_answer = [entry['id'] for entry in dict_tips if entry['id'] not in self.id_dict_tips]
            if len(possible_answer) == 0:
                self.id_dict_tips.clear()   
                possible_answer = [entry['id'] for entry in dict_tips]
        else:   
            #Si ya se han mostrado todas las respuestas, se empieza de nuevo
            self.id_dict_tips.clear()   
            possible_answer = [entry['id'] for entry in dict_tips]
        #Respuesta comprendida como posible respuesta
        key_value = random.choice(possible_answer)
        answer = next((sub for sub in dict_tips if sub['id'] == key_value), None)['desc']
        self.id_dict_tips.append(key_value)
                
        return answer


    # Se llama a Question Answering para procesar la entrada del usuario
    async def run_question_answering(self, turn_context : TurnContext) -> object:
        #Respuesta de Question Answering
        answer = None
        if self.last_output is None:
            self.last_output = self.question_anwering.get_answers(
                confidence_threshold = 0.5,
                question = turn_context.activity.text,
                project_name = "QAnswering-TFG",
                deployment_name = "production"
            )
        else:
            self.last_output = self.question_anwering.get_answers(
                confidence_threshold = 0.5,
                question = turn_context.activity.text,
                answer_context=models.KnowledgeBaseAnswerContext(
                        previous_qna_id=self.last_output.answers[0].qna_id
                    ),
                project_name = "QAnswering-TFG",
                deployment_name = "production",
            )
        
        if self.last_output is not None:
            if self.last_output.answers[0].confidence >= 0.5:
                if self.last_output.answers[0].dialog.prompts is None:
                    answer = self.last_output.answers[0].answer
                else:
                    if turn_context.activity.channel_id.strip() == "emulator":
                        answer= MessageFactory.suggested_actions(self.create_card_action(self.last_output.answers[0]),self.last_output.answers[0].answer)
                    else:
                        answer = self.create_hero_card(self.last_output.answers[0])
        return answer


