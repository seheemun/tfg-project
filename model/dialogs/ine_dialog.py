from botbuilder.dialogs import (
    WaterfallDialog,
    WaterfallStepContext,
    DialogTurnResult,
)
from botbuilder.dialogs.prompts import (
    TextPrompt,
    ChoicePrompt,
    PromptOptions,
    PromptValidatorContext
)
from botbuilder.dialogs.choices import Choice
from botbuilder.core import MessageFactory

from model import INESearch, ine_code_description, ine_all_searches
from .cancel_and_help_dialog import CancelAndHelpDialog
from persistence import ine_manager
from persistence.ine_manager import INESearchDAOImp, INESearchDTO

from azure.cognitiveservices.language.luis.runtime.models import LuisResult

from botbuilder.core import (
    MessageFactory,
)

from botbuilder.schema import (
    ActivityTypes,
    Activity,
    Attachment
)

import re
import datetime
from dateutil.parser import parse
from datetime import date
import datetime
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import uuid as uid
import base64
import os.path
import datetime
import random


class INEDialog(CancelAndHelpDialog):
    def __init__(self, luis_result : LuisResult):
        super(INEDialog, self).__init__(INEDialog.__name__)

        self.luis_result = luis_result
        self.ine_search = None

        self.add_dialog(
            WaterfallDialog(
                WaterfallDialog.__name__,
                [
                    self.search_step,
                    self.date_step,                
                    self.answer_step,
                    self.plot_step,
                ],
            )
        )
        self.add_dialog(TextPrompt(TextPrompt.__name__,self.input_prompt_validator))
        self.add_dialog(ChoicePrompt(ChoicePrompt.__name__))
        self.initial_dialog_id = WaterfallDialog.__name__



    async def search_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        search_entity = self.get_search_data()
        date_entity = list(dict.fromkeys(self.get_date_data()))
        
        self.ine_search = INESearch(search_entity, date_entity)

        if search_entity:
            return await step_context.next(search_entity)
        return await step_context.prompt(
        TextPrompt.__name__,
        PromptOptions(prompt=MessageFactory.text("¿Qué datos te interesa conocer del INE?"),
        retry_prompt=MessageFactory.text("Mmm... ¿Podrías reformularlo?"),
        validations=self.ine_search,
        ),)


    async def date_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:

        step_context.values["search"] = self.ine_search.get_search() 
        date_entity = list(dict.fromkeys(self.ine_search.get_years()))

        if date_entity or any(val for val in step_context.values["search"] if val in ine_all_searches):
            date_entity.sort()
            return await step_context.next(date_entity)  
        return await step_context.prompt(
            TextPrompt.__name__,
            PromptOptions(prompt=MessageFactory.text("¿En qué año?"),
            retry_prompt=MessageFactory.text("Esto... ¿Podrías repetirme el año de otra manera?"),
            validations=self.ine_search),
        )


    async def answer_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        step_context.values["date"] = self.ine_search.get_years() 
 
        err = 0
        if self.ine_search.get_years():
            answer, err = self.create_answer(step_context.values["date"], step_context.values["search"])
        else:
            answer = "Te voy a hacer un dibujo..."
            err = 2
        await step_context.context.send_activity(MessageFactory.text(answer))

        step_context.values["listed_values"] = self.ine_search.get_listed_values()
        step_context.values["chart_description"] = self.ine_search.get_description().capitalize()

        if err == 0:
            if random.randint(0, 2) == 0:
                if (
                (any(val for val in step_context.values["search"] if val in ine_all_searches) or
                any(val for val in step_context.values["search"] if val in ine_code_description)
                )
                and 
                (
                (len(self.ine_search.get_listed_values()) > 3 or not step_context.values["date"]) or
                (len(step_context.values["search"]) == 1 and len(self.ine_search.get_listed_values()) > 3)
                )
                ):
                    return await step_context.prompt(
                    ChoicePrompt.__name__,
                    PromptOptions(
                        prompt=MessageFactory.text("¿Quieres que te haga un pequeño dibujo? Así puede que te hagas mejor la idea"),
                        retry_prompt=MessageFactory.text("¿Quieres que te lo dibuje o no?"),
                        choices=[Choice("Sí"), Choice("No")],
                        ),
                    )
        elif err == 2:
            return await step_context.next(Choice("Sí"))      
        return await step_context.next(Choice("No"))  


    async def plot_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
       
        last_answer = step_context.result.value.lower()

        if last_answer != "no":
            if (any(val for val in step_context.values["search"] if val in ine_all_searches) and 
                (len(self.ine_search.get_listed_values()) > 3 or not step_context.values["date"])):
                await step_context.context.send_activity(MessageFactory.text("Ahí va un pequeño gráfico para que te hagas mejor la idea, los datos que he usado son todos los disponibles del INE hasta día de hoy"))

                if "victima" in step_context.values["search"]:
                    # numero de victimas
                    self.create_predefined_chart(1)
                    chart_id="img1.png"
                    await step_context.context.send_activity(MessageFactory.text("La violencia de género se puede ver potenciada por varios factores, \
                    entre ellos los socioculturales como la existencia de una cultura patriarcal que fomente las desigualdades entre géneros, \
                    el escaso apoyo institucional a las víctimas o la insuficiente respuesta judicial hacia los maltratadores, \
                    entre los factores individuales de los agresores se podría mencionar la interiorización de un modelo de masculinidad estereotipado \
                    y un proceso de socialización sexista, donde el varón puede incorporar conductas misóginas o posesivas. \
                    La situación de dependencia económica de las víctimas y la existencia de hijos e hijas a los que sustentar es también un agravante en estos casos."))
                
                elif "relationship" in step_context.values["search"]:
                    # que relacion tenia la victima con el denunciado
                    await step_context.context.send_activity(MessageFactory.text("Actualmente en la legislación española, sólo se considera  \
                    violencia de género si existe algún vínculo afectivo entre la víctima \
                    y el agresor. Con este gráfico puedes echar un ojo a las relaciones \
                    entre ambas personas"))
                    self.create_predefined_chart(2)
                    chart_id="img2.png"
                
                elif any(val in step_context.values["search"] for val in ["absuelto","condenado","denunciado"]):
                    # numero de absueltos
                    self.create_predefined_chart(3)
                    chart_id="img3.png"
                    await step_context.context.send_activity(MessageFactory.text("Con el siguiente gráfico podrás ver la relación entre condenados \
                    y absueltos por algún delito de violencia de género"))

                elif "infraction" in step_context.values["search"]:
                    # que tipos de infracciones hay por violencia de genero
                    self.create_predefined_chart(4)
                    chart_id="img4.png"
                    await step_context.context.send_activity(MessageFactory.text("Las infracciones por violencia de género tienen distinta clasificación, \
                    echa un ojo al gráfico para hacerte una idea"))
                elif "nationality" in step_context.values["search"]:
                    # que nacionalidad tienen los denunciados
                    self.create_predefined_chart(5)
                    chart_id="img5.png"
                    await step_context.context.send_activity(MessageFactory.text("Los números no mienten, en España, la mayor parte de los agresores son españoles, \
                    y no inmigrantes o extrangeros como a veces se pregona "))
                
            else:
                chart_id = self.create_user_chart(step_context.values["listed_values"], step_context.values["chart_description"])
                await step_context.context.send_activity(MessageFactory.text("Ahí va. Los datos son los mismos de arriba, pero de forma gráfica"))

            reply = Activity(type=ActivityTypes.message)
            reply.text = step_context.values["chart_description"]
            reply.attachments = [await self.get_inline_attachment(chart_id)]

            await step_context.context.send_activity(reply)
              
        return await step_context.end_dialog()


    def create_user_chart(self, data : dict, chart_title : str):
        fig = plt.figure(layout="constrained")
        sns.set_theme(style="whitegrid")
        chart_id = str(uid.uuid4()) + ".png" 
        vals = [int(i) for i in data.values()]
        chart = sns.barplot(x=list(data.keys()),y=vals, palette="rocket")
        chart.set(xlabel ="Año")
        plt.title(chart_title)
        fig.subplots_adjust(top=0.78, bottom=0.28)
        plt.savefig("resources/" + chart_id)   
        plt.close(fig)
        return chart_id



    async def input_prompt_validator(self, prompt_context: PromptValidatorContext) -> bool:

        self.luis_result = await self.recognizer.recognize(prompt_context.context)
        validation_search = prompt_context.options.validations
        search_entity = validation_search.get_search()
        date_entity = validation_search.get_years()
        
        if not search_entity:
            search_entity = self.get_search_data()    
            if not search_entity:
                if prompt_context.options.number_of_attempts > 2:
                    await prompt_context.context.send_activity("Porfa, dilo de una manera más clara, que yo no entender muy bien 😅")
                    prompt_context.options.number_of_attempts = 0
                else:
                    await prompt_context.context.send_activity("Esto.. ¿Me puedes repetir la pregunta de otra manera?")
                return False
            else:
                self.ine_search.set_search(search_entity)
        if search_entity:
            if not date_entity:
                date_entity = list(dict.fromkeys(self.get_date_data())) 
                if not date_entity:
                    if prompt_context.options.number_of_attempts > 2:
                        await prompt_context.context.send_activity("Amig@... Solo tienes que decirme un año y ya, no te enrolles :D")
                        prompt_context.options.number_of_attempts = 0
                    else:
                        await prompt_context.context.send_activity("¿De qué año quieres saberlo?")
                    return False
                else:
                    self.ine_search.set_years(date_entity)

        return True


    def get_search_data(self):

        add_nationality = False
        main_search_entity = []
        all_searches = []

        for entity in self.luis_result.properties['luisResult'].entities: 
            if entity.type == 'ineQuestion.nationality':
                add_nationality = True
            elif entity.type == 'ineQuestion.search':
                main_search_entity.append(entity.entity)

            if "resolution" in entity.additional_properties: 
                if entity.type in ["infractionType","relationshipType"]:
                    all_searches.append(entity.additional_properties['resolution']["values"][0])
                elif entity.type in ["nationalityList"]:
                    if add_nationality:
                        all_searches.append(entity.additional_properties['resolution']["values"][0])
                elif entity.type in ["searchList"]:
                    if entity.entity in main_search_entity:
                        all_searches.append(entity.additional_properties['resolution']["values"][0])

            if 'children' in entity.additional_properties:
                for property in entity.additional_properties['children']:
                    if property['type'] in ("exRelationship","inRelationship"):
                        all_searches.append(property['type'])
                    elif property['type'] in ("relationship","nationality","infraction"):
                        all_searches.append(property['type'])
                        for value in ine_all_searches.get(property['type']):
                            all_searches.append(value)
                                             
                    # entidad AA con entructura anidada
                    if 'children' in property:
                        if property['children'][0]['type'] in ("conyugue","novio","pareja de hecho"):
                            all_searches.append(property['children'][0]['type'])

        return list(dict.fromkeys(all_searches))



    def get_date_data(self):
        dates = []
        time_years = []
        for entity in self.luis_result.properties['luisResult'].entities: 
            if "resolution" in entity.additional_properties:
                for property_res in entity.additional_properties['resolution']["values"]:
                    if "type" in property_res:
                        if property_res["type"] == "daterange":
                            time_val = property_res["timex"] 
                            time_val = property_res["timex"].replace("(","").replace(")","").split(",")
                            for val in time_val:
                                if self.is_date(val):
                                    if len(val) == 10:
                                        time_years.append(datetime.datetime.strptime(val, '%Y-%m-%d').strftime("%Y"))
                                    elif len(val) == 4:
                                        time_years.append(val)
                                else:
                                    if re.search("P[123456789].", val):
                                        time_years.append(val)

                            dates.extend(time_years)

                            if 'Mod' in property_res:
                                if property_res["Mod"] not in dates and re.search("P[123456789].", ' '.join(str(element) for element in dates)) is None: 
                                    dates.append(property_res["Mod"])
        return dates
  

    """
        Permite saber si el string se puede interpretar como Date

        :param string: str, cadena a comprobar
        :param fuzzy: bool, ignora caracteres extraños si True
    """
 
    def is_date(self, string, fuzzy=False):
        try: 
            parse(string, fuzzy=fuzzy)
            return True

        except ValueError:
            return False


    def create_answer(self, dates : list, searches : list):
            
            err = 0
            answer = ""
            exist_date = any(date for date in dates if isinstance(int(date),int))
            
            for i, search in enumerate(searches):
                if search in ine_code_description and exist_date:
                    ine_code = ine_code_description.get(search).get('code') 
                    ine_desc = ine_code_description.get(search).get('desc')
                    
                    #ine_values, val1 = ine_manager.get_ine_value(ine_code,dates)

                    ine_dao = INESearchDAOImp()

                    ineSearch_dto = INESearchDTO(ine_code,dates)

                    value_dto = ine_dao.get(ineSearch_dto.get_structure_for_search(),dates)
                    ine_values = value_dto.get_values()
                    val1 = value_dto.get_val_aux()

                    self.ine_search.set_description(ine_desc)
                    self.ine_search.set_listed_values(val1)

                    if i == 0 or not answer:
                        if re.search("P[123456789].", ' '.join(str(element) for element in dates)):
                            answer = f"desde el año {dates[0]} hasta el {dates[-2]}, se han registrado "
                        elif "since" in dates:
                            if str(dates[0]) >= str(date.today().year):
                                answer = f"hasta el año {dates[0]}, se han registrado "
                            else:
                                answer = f"desde el año {dates[0]}, se han registrado "
                        elif "before" in dates:
                            answer = f"hasta el año {dates[0]}, se han registrado "
                        else:
                            if len(dates) == 1:
                                answer = f"Para el año {dates[0]}, se han registrado "
                            else:
                                if "P0Y" in dates:
                                    answer = 'en el año {} se han registrado '.format(', '.join(dates[:-2]))
                                if "P1Y" in dates:
                                    answer = 'en los años {} y {} se han registrado respectivamente '.format(', '.join(dates[:-2]), dates[-2])
                                else:
                                    if len(ine_values) == 1:
                                        answer = 'en los años {} y {} se han registrado un total de '.format(', '.join(dates[:-1]), dates[-1])
                                    else:
                                        answer = 'en los años {} y {} se han registrado respectivamente '.format(', '.join(dates[:-1]), dates[-1])
                    for j, ine in enumerate(ine_values):
                        if ine > 0:
                            answer += f"{ine} "   
                        else:
                            answer += f"0 casos (o no existen estadísticas) de "   
                        if j == len(ine_values) -2:
                            answer += " y "
                        else:
                            if j != len(ine_values) -1: 
                                answer += ", "

                    answer += f"casos de {ine_desc}"  

                    if i == len(searches) -2:
                        answer += " y "
                    else:
                        if i != len(searches) -1: 
                            answer += ", "     
                    answer = answer.capitalize()
            if len(val1) == 0:
                answer = "Mmm... creo que aun no te puedo dar esa respuesta. Prueba a preguntarme otra cosa"  
                err = 1
            return answer, err


    def get_ine_long_data(self, dict_id : int):

        aux_dict = {}
        dict_values = {}
        ine_codes = []

        if dict_id == 1:
            values = ["victima","denunciado","condenado"]
            for value in values:
                aux_dict[ine_code_description.get(value).get("code")[0]] = ine_code_description.get(value).get("short_desc")
            ine_codes.append(aux_dict)

        elif dict_id == 2:
            for values in ine_all_searches.get("relationship"):
                for value in ine_code_description.get(values).get("code"):
                    dict_values[value] = ine_code_description.get(values).get('short_desc')
            ine_codes.append(dict_values)

        elif dict_id == 3:
            values = ["absuelto","infraccion","condenado"]
            for value in values:
                aux_dict[ine_code_description.get(value).get("code")[0]] = ine_code_description.get(value).get("short_desc")
            ine_codes.append(aux_dict)

        elif dict_id == 4:
            for values in ine_all_searches.get("infraction"):
                for value in ine_code_description.get(values).get("code"):
                    dict_values[value] = ine_code_description.get(values).get('short_desc')
            ine_codes.append(dict_values)

        elif dict_id == 5:
            for values in ine_all_searches.get("nationality"):
                for value in ine_code_description.get(values).get("code"):
                    dict_values[value] = ine_code_description.get(values).get('short_desc')
            ine_codes.append(dict_values)

        #ine_manager.save_ine_data(ine_codes, dict_id, 100)
        ine_dao = INESearchDAOImp()

        ineSearch_dto = INESearchDTO(values=ine_codes)

        ine_dao.create(ine_codes, dict_id, ineSearch_dto.get_structure_for_search_with_codes())



    # Crea 4 gráficas pre definidas
    def create_predefined_chart(self, chart_id : int):

        self.get_ine_long_data(chart_id)

        if chart_id == 1:
            data1 = pd.read_csv('data/data_plot_1.csv')
            sns.set_theme(style="whitegrid")
            fig = plt.figure()
            fig.subplots_adjust(top=0.76, bottom=0.3)
            chart = sns.lineplot(x="anyo", y="numero",hue="dato", style="dato", data=data1,dashes=True, markers=False, palette="rocket")
            
            plt.title("Número de víctimas, denunciados y condenados (España)")
            plt.xlabel("Año")
            plt.ylabel("Número de personas")
            plt.savefig("resources/img1.png", bbox_inches='tight')
            plt.close(fig)          

        elif chart_id == 2:
            data2 = pd.read_csv('data/data_plot_2.csv')
            sns.set_theme(style="whitegrid")
            fig2 = plt.figure()
            fig2.subplots_adjust(top=0.76, bottom=0.3)
            explode = [0.01 for _ in range(0, data2["dato"].nunique() -1)]
            max_index = data2["numero"].idxmax()
            explode.insert(max_index, 0.08)

            sums = data2.groupby(data2["dato"])["numero"].sum()
            chart = plt.pie(sums, labels=sums.index, labeldistance=1.15, colors=sns.color_palette("muted"), 
            autopct = '%0.0f%%',  startangle=80, explode=explode, textprops={'fontsize': 11}, pctdistance=0.75)

            fig2.set_facecolor('lightgrey')

            plt.title("Relación de la víctima con el denunciado (España)")
            plt.axis("equal")
            plt.savefig("resources/img2.png", bbox_inches='tight')
            plt.close(fig2)

        elif chart_id == 3:
            data3 = pd.read_csv('data/data_plot_3.csv')
            sns.set_theme(style="whitegrid")
            fig3 = plt.figure()

            chart = sns.catplot(x="anyo", y="numero",hue="dato", data=data3,kind="bar",palette="magma",height=3,aspect=2)
            chart.set(xlabel ="Año", ylabel = "Nº de personas e infracciones")
            plt.xlim(3.5, None)
            plt.title("Número de condenados, absueltos e infracciones (España)")

            plt.savefig("resources/img3.png", bbox_inches='tight')
            plt.close(fig3)

        elif chart_id == 4:
            data4 = pd.read_csv('data/data_plot_4.csv')
            sns.set_theme(style="whitegrid")

            data4_1 = ["Lesiones","Amenazas","Torturas","Quebrantamiento  de  condena"]
            data4_2 = ["Injurias","Daños","Otros  delitos","Coacción"]
            data4_3 = ["Homicidio","Secuestro","Agresión  sexual","Abuso  sexual", "Allanamiento de morada"]

            data4_1 = data4[data4.dato.isin(data4_1)]
            data4_2 = data4[data4.dato.isin(data4_2)]
            data4_3 =  data4[data4.dato.isin(data4_3)]

            fig4 = plt.figure()
            sns.set_palette('pastel')
            plt.subplot(313)

            a = sns.lineplot(x="anyo", y="numero", data=data4_3, hue="dato")
            a.sharex=True
            a.sharey=True
            plt.legend(bbox_to_anchor=(1.02, 1), loc='upper left', borderaxespad=0)
            a.set(xlabel ="Año", ylabel = None)
            a.set_xlim(2015, data4["anyo"].max())

            for ind, label in enumerate(a.get_xticklabels()):
                if ind % 2 == 0: 
                    label.set_visible(False)
                else:
                    label.set_visible(True)

            sns.set_palette('bright')
            plt.subplot(312)
            b=sns.lineplot(x="anyo", y="numero", data=data4_2, hue="dato",)
            plt.legend(bbox_to_anchor=(1.02, 1), loc='upper left', borderaxespad=0)
            b.set(xlabel =None, ylabel = "Número de delitos")
            b.set_xlim(2015,data4["anyo"].max())
            for ind, label in enumerate(b.get_xticklabels()):
                if ind % 2 == 0: 
                    label.set_visible(False)
                else:
                    label.set_visible(True)

            sns.set_palette('Set2')
            plt.subplot(311)
            c=sns.lineplot(x="anyo", y="numero", data=data4_1, hue="dato")
            plt.xlim(2015,None)
            plt.legend(bbox_to_anchor=(1.02, 1), loc='upper left', borderaxespad=0)
            #c.set_xticklabels([])
            c.set_xlim(2015,data4["anyo"].max())
            c.set(xlabel =None, ylabel = None)
            c.set(title="Tipos de delito por año (España)")
            plt.tight_layout()
            plt.legend(bbox_to_anchor=(1.02, 1), loc=2, borderaxespad=0)
            plt.tight_layout(pad=1.0)

            for ind, label in enumerate(c.get_xticklabels()):
                if ind % 2 == 0: 
                    label.set_visible(False)
                else:
                    label.set_visible(True)

            fig4.subplots_adjust(top=0.79, bottom=0.27)

            plt.savefig('resources/img4.png')
            plt.title("Tipos de delito por año (España)")
            plt.close(fig4)

        elif chart_id == 5:
            data5 = pd.read_csv('data/data_plot_5.csv')
            fig5 = plt.figure()
            sns.set_theme(style="whitegrid")
            sums = data5.groupby(data5["dato"])["numero"].sum()
            fig1, ax1 = plt.subplots()
            ax1.pie(sums, labels=sums.index, autopct='%0.0f%%', colors = sns.color_palette("dark"), startangle=30, wedgeprops = { 'linewidth' : 2, 'edgecolor' : 'white' })
            my_circle = plt.Circle((0,0), 0.7, color='white')
            fig = plt.gcf()
            fig.gca().add_artist(my_circle)
            ax1.axis('equal')  
            plt.title("Procedencia de los denuncionados por violencia de género (España)")
            fig.subplots_adjust(top=0.76, bottom=0.3)
            plt.savefig('resources/img5.png',  bbox_inches='tight')
            plt.close(fig5)



    """
    Crea un adjunto "inline" usando una cadena base64
    :return: Attachment
    """
    async def get_inline_attachment(self, img_url : str) -> Attachment:
        
        with open(os.path.join(os.getcwd(), "resources/" + img_url), "rb") as in_file:
            base64_image = base64.b64encode(in_file.read()).decode()

        return Attachment(
            name=img_url,
            content_type="image/png",
            content_url=f"data:image/png;base64,{base64_image}",       
        )





