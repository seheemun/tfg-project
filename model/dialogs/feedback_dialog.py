from botbuilder.dialogs import (
    WaterfallDialog,
    WaterfallStepContext,
    DialogTurnResult,
    ListStyle
)
from botbuilder.dialogs.prompts import (
    TextPrompt,
    ChoicePrompt,
    PromptOptions,
)
from botbuilder.dialogs.choices import Choice
from botbuilder.core import MessageFactory, UserState

from model.feedback import Feedback
from .cancel_and_help_dialog import CancelAndHelpDialog
from persistence.feedback_manager import FeedbackDAOImp, FeedbackDTO
from model.response import Response


class FeedbackDialog(CancelAndHelpDialog):
    def __init__(self, user_state: UserState):
        super(FeedbackDialog, self).__init__(FeedbackDialog.__name__)
        self.response = Response()
        self.user_feedback_accessor = user_state.create_property("Feedback")

        self.add_dialog(
            WaterfallDialog(
                WaterfallDialog.__name__,
                [
                    self.opinion_step,
                    self.feedback_step,                
                    self.summary_step,
                ],
            )
        )
        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(ChoicePrompt(ChoicePrompt.__name__))
        self.initial_dialog_id = WaterfallDialog.__name__

    async def opinion_step(
        self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        # WaterfallStep siempre termina con la finalización de Waterfall o con otros diálogos;
        # El siguiente WaterfallStep se lanzará cuando se reciba la respuesta del usuario

        await step_context.context.send_activity(MessageFactory.text("No te vayas aun!!"))

        return await step_context.prompt(
            ChoicePrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("Si te ha gustado hablar conmigo, ¿Cuántas ⭐ me pondrías?😳"),
                choices=[Choice("0. 💩"), Choice("1. ⭐"),  Choice("2. ⭐⭐"), Choice("3. ⭐⭐⭐")],
                style=ListStyle.hero_card
            )
        )

    async def feedback_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        step_context.values["rating"] = step_context.context.activity.text
        return await step_context.prompt(
            TextPrompt.__name__,
            PromptOptions(prompt=MessageFactory.text("¿Qué sugerencia de mejora me puedes dar?🙏🙏")),
        )

    async def summary_step(
        self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        step_context.values["feedback"] = step_context.result

        
        # Obtenemos el objeto feedback del estado del usuario
        # Cambios en el objeto se guardarán en el siguiente turno (on_turn)
        user_feedback = await self.user_feedback_accessor.get(step_context.context, Feedback)
        user_feedback.set_feedback(str(step_context.values["feedback"]))
        user_feedback.set_rating(str(step_context.values["rating"]))
        
        fb_dao = FeedbackDAOImp()
        fb_dto = FeedbackDTO(user_feedback.get_id(),user_feedback.get_feedback(),user_feedback.get_rating(),user_feedback.get_date())
        fb_dao.create(fb_dto.get_structure())

        await step_context.context.send_activity(MessageFactory.text(self.response.get_response("thank")))
        return await step_context.end_dialog()

  
