import uuid as uid
from datetime import date

class Feedback:

    """
    Constructor de la clase
    @param self: el propio objeto Feedback
    @param id: identificador único del Feddback
    @param rating: puntuación del bot en una escala numérica fija
    @param feedback: retroalimentación del usuario sobre el bot
    @param date: fecha del feedback recibido 
    """
    
    def __init__(self, rating: int = 0, feedback: str = None):
        self.id = str(uid.uuid4())
        self.rating = rating
        self.feedback = feedback
        self.date = date.today().strftime('%Y-%m-%d %H:%M:%S')

    def get_id(self):
        return self.id

    def get_rating(self):
        return self.rating
    
    def get_feedback(self):
        return self.feedback
    
    def get_date(self):
        return self.date
    
    def set_rating(self, rating):
        self.rating = rating
    
    def set_feedback(self, feedback):
        self.feedback = feedback


    