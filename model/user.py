"""
Clase UserProfile. Representa un usuario en el canal destino.
"""
class UserProfile:

    """
    Constructor de la clase
    @param self: el propio objeto UserProfile
    @param name: nombre del usuari@
    @param like: indica si al usuari@ le ha gustado el bot
    @param feedback: realimentación que proporciona el usuari@ con respecto al funcionamiento del bot
    """
    def __init__(self, name: str = None):
        self.name = name

    #-------- Getters ------------
 
    def get_name(self):
        return self.name

    #-------- Setters ------------   

    def set_name(self, name: str = None):
      self.name = name
