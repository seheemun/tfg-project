
from enum import Enum



class Intent(Enum):
    HELLO = "hello_intent"
    GOODBYE = "goodbye_intent"
    CANCEL = "cancel_intent"
    HELP = "help_intent"
    TERMS_OF_USE = "terms_of_use_intent"
    NONE = "None"
    COMPLIMENT = "compliment_intent"
    CRITICIZE = "criticize_intent"
    FEMINIST_TIP = "feministTip_intent"
    IDENTIFY_SEXISM = "identifySexism_intent"
    GET_USER_NAME = "getName_intent"
    THANK = "thank_intent"
    INE_QUESTION = "ineQuestion_intent"
    INE_INFRACTION = "ineQuestionInfraction_intent"
    INE_NATIONALITY = "ineQuestionNationality_intent"
    INE_RELATIONSHIP = "ineQuestionRelationship_intent"
    QNA = "QnA_intent"



