
import random

class Response:

    def __init__(self):

        self.initiate = ['¡Hola! ¿En qué te puedo ayudar?',
            '¡Buenas! ¿En qué te puedo ayudar?',
            '¡Hola! Pregúntame algo', 
            '¡Hola! ¿Tienes alguna consulta?', 
            'Hola, ¿qué tal todo? Puedes consultarme las preguntas que tengas',
            '¡Hola! Estoy aquí para ayudarte en todo lo necesario. ¿Tienes alguna pregunta para mí?',
            'Hola, ¿Cómo te puedo ayudar?']

        self.terminate = ['Me ha gustado hablar contigo, ¡hasta luego! 😋😋',
            'Chao pescao🐟',
            'Adiós!👋',
            '¡Hasta pronto!👋',  
            '¡Chao!👋👋',
            '¡Hasta luego! 😊',
            'Chaoo!!',
            'Byee👋',
            'Nos vemos😉',
            'Hasta luego!!🤗🤗']

        self.thank = ['¡Gracias!🙌',
            '¡Muchas gracias!',
            'Muchas gracias 😊',
            'Merci beaucoup 😁']

        self.apologize = ['Perdona',
            'Lo siento',
            'Lo siento mucho',
            'Mis disculpas']

        self.not_understood = ['Perdona, no te he entendido bien.😶',
            'No lo he entendido bien, ¿lo puedes repetir?',
            'Lo siento... ¿Podrías reformular la pregunta?🤔',
            'Perdona, ¿Podrías repetirlo?😕']


        self.identify_sexism = [
            {'id': 0, 'desc' : 'Hacer bromas o chistes sobre la violencia sexual o machismo y defenderlos con la expresión "No se puede ni hacer un chiste".'},
            {'id': 1, 'desc' : 'Si eres mujer, y tu novio o marido se siente incómodo si tú tienes un mejor sueldo, o trabajo.'},
            {'id': 2, 'desc' : 'Si eres mujer, y tu marido o novio dice que "ayuda" en las tareas del hogar. Con ello él está asumiendo que la labor de mantener limpia y ordenada la casa es tuya.'},
            {'id': 3, 'desc' : 'Si eres mujer, y tu marido o novio dice que "ayuda" con el cuidado de los hijos. Con ello él está asumiendo que esa labor es tuya.'},
            {'id': 4, 'desc' : 'Si eres varón y no eres capaz de realizar las compras del hogar. Si se da el caso, es porque has asociado que esta labor la debe de realizar la mujer.'},
            {'id': 5, 'desc' : 'Si eres mujer y tu marido o novio pretende que estés a su disposición 24/7.'},
            {'id': 6, 'desc' : 'Si eres hombre y en el transporte público empleas el manspreading ocupando más espacio del que te toca cuando tienes a otras personas a tu lado.'},
            {'id': 7, 'desc' : 'Cuando ves anuncios de limpieza de hogar dirigidos únicamente para la mujer.'},
            {'id': 8, 'desc' : 'Regalar juguetes de super héroes y aventuras a niños, mientras que a las niñas juguetes de cocina y maternidad.'},
            {'id': 9, 'desc' : 'Cuando una mujer y un hombre preguntan algo a alguien y esa persona responde al hombre porque piensa que la mujer no lo va a entender.'},
            {'id': 10, 'desc' : 'Si eres mujer, esperan que te comportes como una señorita.'},
            {'id': 11, 'desc' : 'Las mujeres de la casa son las responsables de atender y servir a los invitados.'},
            {'id': 12, 'desc' : 'Si eres mujer, esperan que te cases, seas madre y también ama de casa.'},
            {'id': 13, 'desc' : 'A la figura paterna la ven como el pilar de la familia.'},
            {'id': 14, 'desc' : 'Si eres mujer y tu pareja te pide o exige que te cambies de ropa para no llamar la atención.'},
            {'id': 15, 'desc' : 'Si eres mujer y te dicen que "estás en esos días". Esta es una expresión sexista, que implica que las mujeres solo pueden tener cambios de humor cuando tienen la regla, cuando no la tienen deben ser calmadas y pasivas.'},
            {'id': 16, 'desc' : 'Si eres mujer y tu pareja de presiona o insiste demasiado en tener relaciones sexuales.'},
            {'id': 17, 'desc' : 'Si eres hombre y culpas a la víctima de haber sufrido un abuso sexual. Con comentarios del tipo: "Ella se lo ha buscado", "Con esa ropa qué esperaba"'},
            {'id': 18, 'desc' : 'Normalizar agresiones sexuales con frases del tipo: "Lo hombres son así", "No es para tanto".'},
            {'id': 19, 'desc' : 'Que un niño levante la falda a una niña y se le excuse con la expresión "Son cosas de niños".'}
        ]


        self.feminist_tip = [
            {'id': 0, 'desc' : 'Deja de utilizar adjetivos asociados a genitales femeninos como algo negativo: "Esto es un coñazo" y adjetivos asociados a genitales masculinos como algo positivo: "Esto es la polla".'},
            {'id': 1, 'desc' : 'Abre los ojos y los oídos y reconoce la existencia del patriarcado, como un sistema que nos oprime y genera desigualdades de género.'},
            {'id': 2, 'desc' : 'Como hombre, reconoce que tienes privilegios sobre las mujeres.'},
            {'id': 3, 'desc' : 'Acepta que la palabra de una mujer es tan válida como la de un hombre. Las mujeres también piensan y toman conclusiones como los hombres.'},
            {'id': 4, 'desc' : 'Si una persona te dice que eres machista, escúchala. Seguramente tenga razón.'},
            {'id': 5, 'desc' : 'Si eres hombre, comparte las tareas domésticas y no lo hagas por "ayudar", ya que si ayudas estás asumiendo que el trabajo es de la mujer.'},
            {'id': 6, 'desc' : 'A la mujer no se le da mejor que al hombre, limpiar, cocinar o cuidar. Por ello, si eres hombre, no lo utilices como excusa para no realizar estas tareas.'},
            {'id': 7, 'desc' : 'Entiende que una mujer, por ser mujer, no tiene que tener como un objetivo en la vida, tener hijos o casarse; estos son los objetivos inculcados tradicionalmente a las mujeres.'},
            {'id': 8, 'desc' : 'Si presencias comportamientos machistas de tus conocidos o amigos, no mires hacia otro lado.'},
            {'id': 9, 'desc' : 'Las mujeres constituyen más de la mitad de la población mundial, así que en cada espacio donde la mujer no sea la mitad, cuestiónate el por qué.'},
            {'id': 10, 'desc' : 'No es No. Si te ha dicho que no, no insistas. Las mujeres saben lo que quieren.'},
            {'id': 11, 'desc' : 'Si eres hombre, trata a las mujeres como tus iguales.'},
            {'id': 12, 'desc' : 'Practica la empatía y autocrítica.'},
            {'id': 13, 'desc' : 'Si eres hombre, detecta y rechaza mecanismos de defensa masculinos y propios de agresores machistas.'},
            {'id': 14, 'desc' : 'Si eres hombre, no niegues tu culpa en una agresión sexual, no intentes culpabilizar a la víctima, justificar tus acciones o minimizar el daño causado en ella.'},
            {'id': 15, 'desc' : 'Cuestionate los roles de género. Toda connotación que se puede atribuir a un rol femenino o masculino, no tiene ninguna relación con los genitales. Es decir, las características predominantes de los géneros son una construcción e imposición cultural.'},
            {'id': 16, 'desc' : 'No naturalizar el abuso hacia la mujer, o justificarlo.'},
            {'id': 17, 'desc' : 'Escucha las experiencias de otras mujeres.'},
            {'id': 18, 'desc' : 'Educa en la igualdad de género a los niños y niñas.'},
            {'id': 19, 'desc' : 'No limites, ni te limites, a realizar actividades, estudios o trabajos en base al género que tengas.'},
            {'id': 20, 'desc' : 'Materniza la paternidad. El hijo o hija no es solo de la madre, comparte actividades que son socialmente asociadas a la madre. Esto se puede ver reflejado en el día a día, cuando hay que cambiar al bebé y el cambiador de pañales se encuentran únicamente en los baños de mujeres. '},
            {'id': 21, 'desc' : 'Asume que llevar a cabo un embarazo es decisión de la mujer, dado que es ella quien lo llevará durante 9 meses, con todo lo que esto implica.'},
            {'id': 22, 'desc' : 'Acepta que una mujer no tiene por qué depilarse, ni mucho menos para que te guste a ti.'},
            {'id': 23, 'desc' : 'Si eres hombre, y vas por la calle detrás de una mujer por la noche, cámbiate de acera para que ella no se sienta perseguida.'},
            {'id': 24, 'desc' : 'No hagas chistes machistas y si algún amigo tuyo lo hace, córtale el rollo.'},
            {'id': 25, 'desc' : 'Asume que las mujeres no necesitan la aprobación de un hombre para hacer lo que quieran.'},
            {'id': 26, 'desc' : 'No juzgues a una mujer por su vida sexual o la ropa que esté vistiendo.'}
        ]

        self.help = "¡Soy FeministBot, el bot Feminista!👩‍💻\n\n\u00A0\n\nPuedes hacerme preguntas sobre el feminismo en general,\
            como: \n\n\u00A0\n\n 🔠 Términos utilizados o acuñados por este movimiento \
            (p.e. ¿Qué es una machoexplicación?) \
            \n\n\u00A0\n\n 💡 Tips feministas para ayudar en la lucha (p.e. Dime un tip feminista) \
            \n\n\u00A0\n\n 🧐 Cómo identificar machismos en tu día a día (p.e. ¿Es machista que mi novio no me deje usar minifalda?) \
            \n\n\u00A0\n\n 📈 Estadísticas y cifras reales del INE (Instituto Nacional de Estadística) \
            sobre la desigualdad y discriminación de género (p.e ¿Según el INE cuántas denuncias de mujeres hubo por violencia machista?) \
            \n\n\u00A0\n\n Para consultar sobre los términos de uso que aplican, sigue este enlace \
            https://www.privacypolicies.com/live/afdcf2ea-22e2-4f23-8654-e6a8dcd69e7f  "    

        self.terms = "En virtud de lo establecido en el Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo, de 27 de abril de 2016, le informamos que el responsable del tratamiento de sus datos es FeministBot (email sehee.mun@estudiantes.uva.es). \n\n\u00A0\n\nLa finalidad del tratamiento de datos será la recogida de forma anónima de la retroalimentación sobre el funcionamiento del software. La base jurídica del tratamiento se encuentra en el artículo 6.1. letras a y b del Reglamento Europeo (existencia de una relación contractual o prenegocial y el consentimiento del interesado). \n\n\u00A0\n\nLos datos se conservarán durante el tiempo necesario para cumplir con la finalidad para la que se obtuvieron. Por otro lado, le informamos de su derecho a ejercer sus derechos de acceso, rectificación o supresión, o la limitación de su tratamiento, o a oponerse al mismo, acreditando su identidad, y remitiendo una comunicación a la dirección: sehee.mun@estudiantes.uva.es. \n\n\u00A0\n\nFinalmente, le recordamos su derecho a requerir, siempre que lo desee, el amparo de la Agencia Española de Protección de Datos.\n\n\u00A0\n\nPara consultar el detalle de este tratamiento puede acceder a: https://www.privacypolicies.com/live/afdcf2ea-22e2-4f23-8654-e6a8dcd69e7f"


        self.cancel = ["🤐","😶","👀","🥺","💩","🥱"]

        self.options = {
        'initiate': self.initiate,
        'terminate': self.terminate,
        'thank': self.thank,
        'apologize': self.apologize,
        'not_understood': self.not_understood,
        'identify_sexism': self.identify_sexism,
        'feminist_tip': self.feminist_tip,
        'help': self.help,
        'cancel': self.cancel,
        'terms': self.terms
        }

    
    """
    Devuelve la respuesta solicitada
    @param name: Nombre de respuesta
    @param return_all: Devolver toda la estructura (True)
    @param value: Valor (int) o random (None)
    """
    def get_response(self, name : str, return_all : bool = False, value : int = None):

        obj = self.options[name]

        if not return_all:
            if isinstance(obj, list):
                if isinstance(obj[0], dict):
                    if not value:
                        return random.choice([values['desc'] for values in obj])
                    else:
                        return list(obj[value].values())[1]
                else:
                    if not value:
                        return random.choice(obj)
                    else:
                        return obj[value]               
            else:
                return obj
        return obj
    