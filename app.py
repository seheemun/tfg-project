
import sys
import traceback
from datetime import datetime
import time
from typing import Dict
import asyncio
import threading
from http import HTTPStatus
from aiohttp.web import Request, Response, json_response
from aiohttp import web
from botbuilder.core import (
    BotFrameworkAdapterSettings,
    BotFrameworkAdapter,
    TurnContext,
    MemoryStorage,
    TurnContext,
    UserState,
    ConversationState,
    ActivityHandler
)
from botbuilder.core.integration import aiohttp_error_middleware
from botbuilder.schema import Activity, ConversationReference, ChannelAccount, ActivityTypes

from controller.dialog_controller import DialogController 
from model.conversation import *
from config.config import DefaultConfig


class BotViewController(ActivityHandler):

    def __init__(self, conversation_state: ConversationState, user_state: UserState, 
        conversation_references: Dict[str, ConversationReference], bot_conversation_state : BotConversation): 
        self.conversation_state = conversation_state
        self.user_state = user_state
        self.conversation_references = conversation_references
        self.bot_conversation_state = bot_conversation_state
        self.dialog_controller = DialogController(self.conversation_state, self.user_state, self.bot_conversation_state)    

    
    async def on_message_activity(self, turn_context: TurnContext):
        await self.dialog_controller.continue_running_dialog(turn_context)
  

    async def on_members_added_activity(
        self,
        members_added: ChannelAccount,
        turn_context: TurnContext
    ):
        for member_added in members_added:
            if member_added.id != turn_context.activity.recipient.id:
                saludo = self.dialog_controller.process_member_added()
                await turn_context.send_activity(saludo)
                              

    def add_conversation_reference(self, activity: Activity):
        """ 
        Inserta referencias de conversaciones a un dictionario
        :param activity: Activity
        """
        conversation_reference = TurnContext.get_conversation_reference(activity)
        self.conversation_references[conversation_reference.user.id] = conversation_reference


    async def on_conversation_update_activity(self, turn_context: TurnContext):
        self.add_conversation_reference(turn_context.activity)
        return await super().on_conversation_update_activity(turn_context)


    async def on_turn(self, turn_context: TurnContext):
        # Guarda cambios que ocurrieron durante el turno
        await super().on_turn(turn_context)
        await self.conversation_state.save_changes(turn_context)
        await self.user_state.save_changes(turn_context)



class AdapterWithErrorHandler(BotFrameworkAdapter):
    def __init__(
        self,
        settings: BotFrameworkAdapterSettings,
        conversation_state: ConversationState,
    ):
        super().__init__(settings)
        self._conversation_state = conversation_state

        async def on_error(context: TurnContext, error: Exception):
            # This check writes out errors to console log
            # NOTE: In production environment, you should consider logging this to Azure
            #       application insights.
            print(f"\n [on_turn_error] unhandled error: {error}", file=sys.stderr)
            traceback.print_exc()


            await context.send_activity("Oops... Algo ha ido mal, vuelve a intentarlo más tarde.")
            # Send a trace activity if we're talking to the Bot Framework Emulator
            if context.activity.channel_id == "emulator":
                # Create a trace activity that contains the error object
                trace_activity = Activity(
                    label="TurnError",
                    name="on_turn_error Trace",
                    timestamp=datetime.utcnow(),
                    type=ActivityTypes.trace,
                    value=f"{error}",
                    value_type="https://www.botframework.com/schemas/error",
                )
                await context.send_activity(trace_activity)

            # Clear out state
            nonlocal self
            await self._conversation_state.delete(context)

        self.on_turn_error = on_error


# Clase que lanza el bot
class App(object):

    def __init__(self):

        self.config = DefaultConfig()
        # Create adapter
        self.settings = BotFrameworkAdapterSettings(self.config.APP_ID, self.config.APP_PASSWORD)
        
        # Create MemoryStorage, UserState and ConversationState
        self.memory = MemoryStorage()
        self.conversation_state = ConversationState(self.memory)
        self.user_state = UserState(self.memory)

        self.adapter = AdapterWithErrorHandler(self.settings, self.conversation_state)
        
        # Create a shared dictionary.  The Bot will add conversation references when users
        # join the conversation and send messages.
        self.conversation_references: Dict[str, ConversationReference] = dict()
        self.last_activity = time.time()

        self.bot_conversation_state = BotConversation(datetime.now())
        self.bot_conversation_state.set_state(3)

        # Create the Bot
        self.bot = BotViewController(self.conversation_state, self.user_state, \
                                    self.conversation_references, self.bot_conversation_state)
        self.app = web.Application(middlewares=[aiohttp_error_middleware])
        self.app.router.add_post("/api/messages", self.messages)

    
    # Listen for incoming requests on /api/messages
    async def messages(self, req: Request) -> Response:
        # Main bot message handler.
        print("Ejecutando BOT")
        if "application/json" in req.headers["Content-Type"]:
            body = await req.json()
        else:
            return Response(status=HTTPStatus.UNSUPPORTED_MEDIA_TYPE)
        activity = Activity().deserialize(body)
        auth_header = req.headers["Authorization"] if "Authorization" in req.headers else ""
        try:          
            response = await self.adapter.process_activity(activity, auth_header, self.bot.on_turn)
            self.last_activity = time.time()
            if response:
                return json_response(data=response.body, status=response.status)
            return Response(status=201)
        except Exception as exception:
            raise exception


    async def send_proactive_message(self):
        while True:
            if self.conversation_references.keys():
                # El usuario no se ha despedido
                if self.bot_conversation_state.get_state() not in (BotConversationState.FINISHED.value, BotConversationState.PAUSE.value):
                    actual_time = time.time()
                    min_time = self.config.MIN_EXPIRE_AFTER_SECONDS
                    max_time = self.config.MAX_EXPIRE_AFTER_SECONDS
                    #Se añade multihilo para evitar tener un while loop corriendo segundo a segundo en el flujo principal, desde el ultimo minuto de actividad del usuario
                    #hasta que se cumplan los X minutos de tiempo máximo
                    if actual_time - self.last_activity >= min_time and actual_time - self.last_activity <= max_time:
                        for conversation in self.conversation_references.values():
                            await self.adapter.continue_conversation(
                            conversation,
                            lambda turn_context : turn_context.send_activity("¡Holaa! ¿Sigues ahí? 😕"),
                            bot_id = self.config.APP_ID #self.config.APP_ID,
                        )
                        break


    def target_callback(self):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        loop.run_until_complete(self.send_proactive_message())



if __name__ == "__main__":
    try:
        bot_app = App()
        thread = threading.Thread(target=bot_app.target_callback, daemon=True)
        thread.start()
        # dejar localhost para pruebas en local
        # web.run_app(bot_app.app, host="localhost", port=bot_app.config.PORT) 
        web.run_app(bot_app.app,port=bot_app.config.PORT)
    except Exception as error:
        raise error
    except KeyboardInterrupt as ki_error:
        raise ki_error

  




