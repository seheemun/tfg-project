#!/usr/bin/env python3

import os
from azure.core.credentials import AzureKeyCredential

""" 
DefaultConfig: Acceso a variables de entorno, en caso de no existir, se toma el valor pasado como parámetro
"""

class DefaultConfig():

    PORT = 8000
    APP_ID = os.environ.get("MicrosoftAppId", "407c931f-2c03-45a0-89c6-594439f736a0")                   
    APP_PASSWORD = os.environ.get("MicrosoftAppPassword", "HQw8Q~Q4pbtxyKqcvZUFt0rCCqCTSFsqwpHt3bg~")   
    LUIS_APP_ID = os.environ.get("LuisAppId","68a61663-24f1-45a7-9ffd-4e5fbe5fc6c0")
    LUIS_API_KEY = os.environ.get("LuisAPIKey","16b2572299584a65a353bc4714ff295d") 
    LUIS_API_HOST_NAME = os.environ.get("LuisAPIHostName","https://luis-feministbot.cognitiveservices.azure.com")
    QNA_PROJECT_ID = os.environ.get("QNA_PROJECT_ID", "QAnswering-TFG")
    QNA_ENDPOINT_KEY = AzureKeyCredential(os.environ.get("QnAEndpointKey", "89091f665fff41f0b8afb69a2325e0ee"))
    QNA_ENDPOINT_HOST = os.environ.get("QnAEndpointHostName", "https://question-answering-tfg.cognitiveservices.azure.com/")
    QNA_DEPLOYMENT = "production"
    EXPIRE_AFTER_SECONDS = os.environ.get("ExpireAfterSeconds",10)
    MAX_EXPIRE_AFTER_SECONDS = os.environ.get("MaxExpireAfterSeconds",250)
    MIN_EXPIRE_AFTER_SECONDS = os.environ.get("MinExpireAfterSeconds",100)
    SLEEP_TIME = os.environ.get("SleepTime",130)

    DB_HOST = os.environ.get('ACCOUNT_HOST', 'https://azurecosmosbd-account.documents.azure.com:443/')
    DB_MASTER_KEY = os.environ.get('ACCOUNT_KEY', 'RuAMbjgeftXmystNi3tbTTOL1kHAPzzuU3Vj6s4gqw9TiHhpNkwvV8LU9Rv06knqK29GMp0SSwSEACDb2dBSyQ==')
    DB_ID = os.environ.get('COSMOS_DATABASE', 'FeministBotDB')
    DB_CONTAINER_ID = os.environ.get('COSMOS_CONTAINER', 'cosmosdb-tfg')