
import os.path
import random
import json

from azure.ai.language.questionanswering import models, QuestionAnsweringClient
from botbuilder.ai.luis import LuisApplication, LuisRecognizer, LuisPredictionOptions
from azure.cognitiveservices.language.luis.runtime.models import LuisResult
from botbuilder.dialogs import DialogSet, DialogTurnStatus, Dialog
from botbuilder.schema import (
    Attachment,
    Attachment
)


from botbuilder.core import (
    ConversationState,
    MessageFactory,
    UserState,
    TurnContext,
    StatePropertyAccessor
)

from model.dialogs import (FeedbackDialog, IntroAndHelpDialog, MainDialog, INEDialog)
from model import (BotConversation, Response, Intent)

from config.config import DefaultConfig

_intent_component_dialogs = ["goodbye_intent", "ineQuestion_intent","ineQuestionInfraction_intent","ineQuestionNationality_intent","ineQuestionRelationship_intent"]
_settings = DefaultConfig()

"""
Controlador centralizado de diálogos y sus estados. Único punto de entrada y lanzamiento de diálogos.
"""
class DialogController:

    # Diálogos que pueden crear un diálogo de componente
    def __init__(self, conversation_state: ConversationState,  user_state: UserState, bot_conversation_state : BotConversation):
    
        self.recognizer = self.create_recognizer(_settings)
        self.question_anwering = QuestionAnsweringClient(_settings.QNA_ENDPOINT_HOST, _settings.QNA_ENDPOINT_KEY)
        
        self.conversation_state = conversation_state
        self.user_state = user_state
        self.bot_conversation_state = bot_conversation_state
        self.accepted_terms = conversation_state.create_property("TermsAccepted")
        self.dialog_state_property = conversation_state.create_property("DialogState")
        self.dialog_set = DialogSet(self.dialog_state_property)
        self.modelo = Response()
        self.dialog_context = None
        self.dialog = None
        

    # Continúa con la ejecución de los diálogos o con el flujo predeterminado
    async def continue_running_dialog(self, turn_context : TurnContext):
        self.dialog_context = await self.dialog_set.create_context(turn_context)
        results = await self.dialog_context.continue_dialog()

        # Es el (1er) mensaje de términos y condiciones
        if await self.accepted_terms.get(turn_context) is None:
            if  turn_context.activity.text == "Acepto":
                # Si ha aceptado los términos se lanza el diálogo de bienvenida
                await self.accepted_terms.set(turn_context, "Acepto")
                self.dialog = IntroAndHelpDialog(self.user_state)
                if not await self.dialog_set.find(self.dialog.id):
                    self.dialog_set.add(self.dialog)  

                await self.run_dialog(self.dialog, turn_context, self.dialog_state_property,)

            else:
                await turn_context.send_activity(self.process_member_added())
                pass
        elif (await self.accepted_terms.get(turn_context,0)) == "Acepto":
            if results.status == DialogTurnStatus.Complete and self.dialog:
                # Limpiamos el stack
                await self.dialog_context.end_active_dialog(self.dialog.id)
                self.dialog = None
            else:
                # Si no hay ningún diálogo en ejecución, seguimos con el diálogo principal
                if results.status != DialogTurnStatus.Waiting: 
                    if turn_context.activity.text:
                        if len(turn_context.activity.text) < 1000:
                            luis_result = await self.recognizer.recognize(turn_context)
                            intent = self.recognizer.top_intent(luis_result, min_score = 0.70)
                            if intent in _intent_component_dialogs:
                                await self.process_intent_dialog(turn_context, intent, luis_result)
                            else:
                                self.dialog = MainDialog(self.question_anwering, self.user_state, turn_context, intent)
                                self.bot_conversation_state.set_state(2)
                                await self.dialog.process_intent()
                        else:
                            self.dialog = MainDialog(self.question_anwering, self.user_state, turn_context, Intent.NOT_UNDERSTOOD.value)
                            self.bot_conversation_state.set_state(2)
                            await self.dialog.process_intent()

    
    # para centralizar los dialogos del dialogSet y llamadas a diálogos se deja aqui
    # Intenciones Luis que podrían generar un diálogo de clase
    async def process_intent_dialog(self, turn_context : TurnContext, intent : str, luis_result : LuisResult):
        if intent == Intent.GOODBYE.value:    
            # Al azar se decide si se preguntará la opinión del usuario o no
            # Si es así, se crea un nuevo diálogo feedback si ya no está creado
            ask_feedback = random.randint(0, 2)
            if ask_feedback == 0:
                self.dialog = FeedbackDialog(self.user_state)
                if not await self.dialog_set.find(self.dialog.id):
                    self.dialog_set.add(self.dialog)  
                await self.run_dialog(
                    self.dialog,
                    turn_context,
                    self.conversation_state.create_property("DialogState"),
                )
            # En caso contrario, se mostrará un mensaje de despedida de los existentes
            else:
                answer = self.modelo.get_response("terminate")
                await turn_context.send_activity(answer)
            self.bot_conversation_state.set_state(1) #finished

        elif intent in (Intent.INE_QUESTION.value, Intent.INE_INFRACTION.value, Intent.INE_NATIONALITY.value, Intent.INE_RELATIONSHIP.value):   
            self.dialog = INEDialog( luis_result)
            if not await self.dialog_set.find(self.dialog.id):
                self.dialog_set.add(self.dialog)  
            await self.run_dialog(
                self.dialog,
                turn_context,
                self.conversation_state.create_property("DialogState"),
            ) 

        else:
            self.dialog = MainDialog(self.question_anwering, self.user_state, turn_context, Intent.NOT_UNDERSTOOD.value)
            self.bot_conversation_state.set_state(2)
            await self.dialog.process_intent()


    def process_member_added(self) -> Attachment:
        return self.create_hero_card_from_attachment()


    def create_hero_card_from_attachment(self)-> Attachment:
        relative_path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(relative_path, "../resources/heroCardTerms.json") 
        with open(path, encoding="ISO8859-3") as in_file:
            card = json.load(in_file)
           
        attach = MessageFactory.list([])
        attach.attachments.append(card)
        return attach
      

    def create_recognizer(self, configuration: DefaultConfig):
        luis_is_configured = (configuration.LUIS_APP_ID and configuration.LUIS_API_KEY and configuration.LUIS_API_HOST_NAME)

        if luis_is_configured:
            luis_application = LuisApplication(
                configuration.LUIS_APP_ID,
                configuration.LUIS_API_KEY,
                configuration.LUIS_API_HOST_NAME,
            )
            luis_option = LuisPredictionOptions(include_all_intents=True, include_instance_data=True, spell_check=True)
            return LuisRecognizer(luis_application, luis_option, True)


    @staticmethod
    async def run_dialog(dialog: Dialog, turn_context: TurnContext, accessor: StatePropertyAccessor):
        dialog_set = DialogSet(accessor)
        dialog_set.add(dialog)

        dialog_context = await dialog_set.create_context(turn_context)
        results = await dialog_context.continue_dialog()
        if results:
            if results.status == DialogTurnStatus.Empty:
                await dialog_context.begin_dialog(dialog.id)
